/*
	MATRIZ v1.0

	Copyright (C) 2009 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

/**
 * @(#)MatMetNum.java
 *
 *
 * @author Jose Nieto
 * @version 1.00 2009/7/8
 *
 *Los metodos ElimGauss, GaussJordan, Montante
 *fueron programados siguiendo el pseudocodigo
 *visto en un documento de la autoria de 
 *M. Valenzuela 2007
 *
 */


public class MatMetNum {

    public MatMetNum() {
    }
    
    public static MatDouble ElimGauss(MatDouble Matriz){
    	//double opc;
    	MatDouble Espejo=new MatDouble(Matriz.r,Matriz.c);
    	//copiando primer linea
    	for (int i = 0; i< Espejo.c; i++)
    		Espejo.mat[0][i]=Matriz.mat[0][i];
    	for (int i = 0; i< Matriz.r; i++){
    		for (int j = i+1; j< Matriz.r; j++){
    			for (int k = 0; k< Matriz.c; k++)
    				Espejo.mat[j][k]=Matriz.mat[j][k]-Matriz.mat[i][k]*Matriz.mat[j][i]*(1/Matriz.mat[i][i]);
    			for (int x = 0; x< Matriz.c; x++)
    				Matriz.mat[j][x]=Espejo.mat[j][x];
    		}
    	}
    	
    	//MatDouble.ImprimeMat(Espejo);
    	
    	for (int i = Matriz.r-1; i>= 0; i--){
    		for (int k = 0; k< Matriz.c; k++)
    			Espejo.mat[i][k]=Matriz.mat[i][k]/Matriz.mat[i][i];
    			
    			//copiar matriz
    			for (int m = 0; m< Matriz.r; m++) {
    				for (int n = 0; n< Matriz.c; n++)
    					Matriz.mat[m][n]=Espejo.mat[m][n];
    			}
    			
    			for (int j = i-1; j>= 0; j--){
    				for (int k = 0; k< Matriz.c; k++)
    					Espejo.mat[j][k]=Matriz.mat[j][k]-Matriz.mat[i][k]*Matriz.mat[j][i];
    					for (int x = 0; x< Matriz.c; x++)
    						Matriz.mat[j][x]=Espejo.mat[j][x];
    		   	}
    	}
    	/*
    	MatDouble x = new MatDouble(z.r,1);
    	for (int k = 0; k< x.r; k++)
    	x.mat[k][0]=z.mat[k][z.c-1];
    	return x;
    	*/
    	MatDouble.ImprimeMat(Matriz);
    	return Matriz;
    }
    
    
    
    public static MatDouble GaussJordan(MatDouble Matriz){
    	MatDouble Espejo = new MatDouble(Matriz.r,Matriz.c);
    	double valor;
    	int renglon;
    	for (int i = 0; i< Matriz.r; i++){
    		
    		///////////////////////////////////////////
    		//encontrando valor maximo
    		valor=Math.abs(Matriz.mat[i][i]);
    		renglon=i;
    	for (int t = i; t< Matriz.r; t++)
    		if(valor<Math.abs(Matriz.mat[t][i])){
    			valor=Math.abs(Matriz.mat[t][i]);
    			renglon=t;
    		}
    		MatDouble.IntercambiaRenglon(Matriz,i,renglon);
    	////////////////////////////////////////////////////
    	
    	
    		for (int k = 0; k< Matriz.c; k++)
    			Espejo.mat[i][k]=Matriz.mat[i][k]/Matriz.mat[i][i];
    			for (int x = 0; x< Matriz.c; x++)
    				Matriz.mat[i][x]=Espejo.mat[i][x];
    				for (int j = 0; j< Matriz.r; j++){
    					if(j!=i){
    						for (int k = 0; k< Matriz.c; k++)
    							Espejo.mat[j][k]=Matriz.mat[j][k]-Matriz.mat[j][i]*Matriz.mat[i][k];
    							for (int x = 0; x< Matriz.c; x++)
    								Matriz.mat[j][x]=Espejo.mat[j][x];
    					}
    				}	
    	}
    	MatDouble.ImprimeMat(Matriz);
    	return Matriz;
    }
    
    public static MatDouble Montante(MatDouble Matriz){
    	double pivAnt=1;
    	double piv=0;
    	MatDouble Espejo = new MatDouble(Matriz.r,Matriz.c);
    	for (int i = 0; i< Matriz.r; i++){
    		piv=Matriz.mat[i][i];
    		for (int j = 0; j< Matriz.r; j++)
    			if(j!=i){
    				for (int k = 0; k< Matriz.c; k++)
    				Espejo.mat[j][k]=(Matriz.mat[j][k]*piv-Matriz.mat[i][k]*Matriz.mat[j][i])/pivAnt;
    				for (int x = 0; x< Matriz.c; x++)
    				Matriz.mat[j][x]=Espejo.mat[j][x];
    			}
    			pivAnt=piv;
    	}
    	MatDouble.MultiplicaEscalarMat(Matriz,1/piv);
    	MatDouble.ImprimeMat(Matriz);
    	return Matriz;
    }
    
    public static MatDouble InversaMat(MatDouble Matriz){
    	MatDouble Ind = MatDouble.IdentidadMat(Matriz.r,Matriz.c);
    	//MatDouble.ImprimeMat(Ind);
    	MatDouble Exp = MatDouble.FusionMat(Matriz,Ind);
    	GaussJordan(Exp);
    	MatDouble result = MatDouble.ExtraerMat(Exp,0,Matriz.c);
    	MatDouble.ImprimeMat(result);
    	return result;
    }
    
    public static MatDouble TriangularInferior(MatDouble Matriz){
    		MatDouble Espejo=new MatDouble(Matriz.r,Matriz.c);
    	//copiando primer linea
    	for (int i = 0; i< Espejo.c; i++)
    		Espejo.mat[0][i]=Matriz.mat[0][i];
    	for (int i = 0; i< Matriz.r; i++){
    		for (int j = i+1; j< Matriz.r; j++){
    			for (int k = 0; k< Matriz.c; k++)
    				Espejo.mat[j][k]=Matriz.mat[j][k]-Matriz.mat[i][k]*Matriz.mat[j][i]*(1/Matriz.mat[i][i]);
    			for (int x = 0; x< Matriz.c; x++)
    				Matriz.mat[j][x]=Espejo.mat[j][x];
    		}
    	}
    	MatDouble.ImprimeMat(Matriz);
    	return Matriz;
    }
    
        public static int RangoMat(MatDouble Matriz){
        	MatDouble MatrizTR=TriangularInferior(Matriz);
        	int contar=0;
    	for (int i = 0; i<MatrizTR.r; i++) {
    		for (int j = 0; j<MatrizTR.c; j++) {
    			if(MatrizTR.mat[i][j]!=0){
    				contar++;
    				j=MatrizTR.c;
    			}
    		}
    	}
    	return contar;
    }
    
    public static double TrazaMat(MatDouble Matriz){
    	double suma=0;
    	for (int i = 0; i<Matriz.r; i++)
    		suma+=Matriz.mat[i][i];
    	return suma;
    }
    
    public static double DeterminanteMat(MatDouble MatrizDet){
    	//Metodo de la matriz Triangular
    	double dato=1;
    	MatDouble MatrizTriang = TriangularInferior(MatrizDet);
    	for (int i = 0; i<MatrizTriang.r; i++){
    		dato=dato*MatrizTriang.mat[i][i];
    	}
    	
    	/**REGLA DE SARRUS 2�y3� orden SOLAMENTE
    	
    	MatDouble Matriz = new MatDouble(MatrizDet.r*2-1,MatrizDet.c);
    	//preparando matriz
    	//repitiendo los renglones
    	
    	for (int i = 0; i<MatrizDet.r; i++){
    		for (int j = 0; j<MatrizDet.c; j++)
    			Matriz.mat[i][j]=MatrizDet.mat[i][j];
    	}
    	
    	for (int i = MatrizDet.r; i<Matriz.r; i++){
    		for (int j = 0; j<Matriz.c; j++){
    		Matriz.mat[i][j]=MatrizDet.mat[i-MatrizDet.r][j];
    		}
    	}
    	   	//MatDouble.ImprimeMat(Matriz);
    	   	
    	int x;
    	double dato=0;
    	
    	double[] a=new double[Matriz.r];
    	double[] b=new double[Matriz.r];
    	
    	//primera parte
    	
    	for (int i = 0; i<MatrizDet.r; i++){
    		x=i;
    		for (int j = 0; j<Matriz.c; j++){
    			
    			if(j==0)
    			dato=Matriz.mat[x][j];
    			else
    				dato=dato*Matriz.mat[x][j];
    				
    			x++;
    			//System.out.println(dato);
    		}
    		a[i]=dato;
    	}
    	
    	//System.out.println("\n");
    	//Segunda parte

    	for (int i = 0; i< MatrizDet.r; i++){
    		x=i;
    		for (int j = Matriz.c-1; j>=0; j--){
    			if(j==Matriz.c-1)
    				dato=Matriz.mat[x][j];
    				else
    					dato=dato*Matriz.mat[x][j];
    				x++;
    				//System.out.println(dato);
    		}
    		b[i]=dato;
    	}
    	dato=MatDouble.SumaArregloTotal(a)-MatDouble.SumaArregloTotal(b);
    	*/
    	System.out.print(dato);
    	
    	return dato;
 
    }
    
    public static MatDouble AdjuntaMat(MatDouble Matriz){
    	MatDouble Inversa=InversaMat(Matriz);
    	MatDouble.MultiplicaEscalarMat(Inversa,DeterminanteMat(Matriz));
    	
    	return Inversa;
    }
    
    public static void main (String[] args) {

    	MatDouble Matriz = new MatDouble(3,3);
    	MatDouble.RellenaMat(Matriz);
    	MatDouble.ImprimeMat(Matriz);
    	MatDouble.ImprimeMat(AdjuntaMat(Matriz));
    	//DeterminanteMat(Matriz);
    	//TriangularInferior(Matriz);
    	//InversaMat(Matriz);
    	//Montante(Matriz);
    	//ElimGauss(Matriz);
    	//GaussJordan(Matriz);
    	//MatDouble.ImprimeMat(Matriz);
    	
    }
    
    
}
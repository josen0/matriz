/*
	MATRIZ v1.0

	Copyright (C) 2009 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

/**
 * @(#)MatDouble.java
 *
 *
 * @author Jose Nieto
 * @version 1.00 2009/7/8
 */
 import java.lang.reflect.Array;
 
public class MatDouble {

    public MatDouble(int r, int c) {
    	this.mat = new double[r][c];
    	this.r=r;
    	this.c=c;
    }
    
    private static void AnadirElemMat(MatDouble Matriz, int r, int c, double valor ){
    	Matriz.mat[r][c]=valor;
    }
    
    public static void RellenaMat(MatDouble Matriz){
    	for (int i = 0; i< Matriz.r ; i++) {
    		for (int j = 0; j< Matriz.c ; j++)
    		AnadirElemMat(Matriz,i,j,FuncIO.LeerTeclaDoubleCMD("Matriz ["+(i+1)+","+(j+1)+"]"));
    		}
    	}
    	
    public static void ImprimeMat(MatDouble Matriz){
    	System.out.println("Matriz ["+Matriz.r+"] ["+Matriz.c+"]");
    	for (int i = 0; i< Matriz.r; i++) {
    		for (int j = 0; j< Matriz.c; j++)
    			System.out.print("\t"+Matriz.mat[i][j]);
    		System.out.print("\n");
    	}
    }
    
    public static MatDouble MultiplicaMat(MatDouble Matriz1, MatDouble Matriz2){
    	if(Matriz1.c==Matriz2.r){
    		MatDouble MatrizResult=new MatDouble(Matriz1.r,Matriz2.c);
    		int sum=0;
    		for (int i = 0; i< MatrizResult.r; i++) {
    			for (int j = 0; j< MatrizResult.c; j++) {
    				sum=0;
    				for (int k = 0; k< Matriz1.c; k++)
    					sum+=Matriz1.mat[i][k]*Matriz2.mat[k][j];
    				AnadirElemMat(MatrizResult,i,j,sum);
    			}
    		}
    		return MatrizResult;
    	}else{
    		System.out.print("Matrices no multiplicables, dimensiones erroneas\n");
    		return null;
    	}
    }
    
    public static MatDouble SumaMat(MatDouble Matriz1, MatDouble Matriz2){
    	if(Matriz1.r==Matriz2.r && Matriz1.c==Matriz2.c){
    		MatDouble Matriz = new MatDouble(Matriz1.r,Matriz2.c);
    		for (int i = 0; i< Matriz.r; i++) {
    			for (int j = 0; j< Matriz.c; j++)
    				AnadirElemMat(Matriz,i,j,Matriz1.mat[i][j]+Matriz2.mat[i][j]);
    		}
    		return Matriz;
    	}else{
    		System.out.print("Dimensiones Incorrectas");
    		return null;
    	}
    }
    
    public static MatDouble IdentidadMat(int r, int c){
    	MatDouble Matriz = new MatDouble(r,c);
    	for(int i=0;i<r;i++){
    		for(int j=0;j<c;j++){
    			if(i==j)
    				AnadirElemMat(Matriz,i,j,1);
    				else
    					AnadirElemMat(Matriz,i,j,0);
    		}
		}
		return Matriz;
    }
    
    public static MatDouble TransponerMat(MatDouble Matriz){
    	MatDouble MatrizT = new MatDouble(Matriz.c,Matriz.r);
    	for (int i = 0; i< MatrizT.r ; i++) {
    		for (int j = 0; j< MatrizT.c ; j++)
    			AnadirElemMat(MatrizT,i,j,Matriz.mat[j][i]);
    	}
    	return MatrizT;
    }
    /*
    public static double DeterminanteMat(MatDouble Matriz){
    	int l;
    	double det;
    	MatDouble MatDet = new MatDouble(Matriz.r,Matriz.c);
    	det=Matriz.mat[0][0];
    	for (int k = 0; k< Matriz.r; k++) {
    		l=k+1;
    		for (int i = l; i< Matriz.r; i++) {
    			for (int j = l; j< Matriz.c; j++) {
    				AnadirElemMat(MatDet,i,j,(Matriz.mat[k][k]*Matriz.mat[i][j]-Matriz.mat[k][j]*Matriz.mat[i][k])/Matriz.mat[k][k]);
    				det*=MatDet.mat[k+1][k+1];
    			}
    		}
    	}
    	return det;
    }
    */
    public static void IntercambiaRenglon(MatDouble Matriz, int r1, int r2){
    	MatDouble Espejo = new MatDouble(Matriz.r,Matriz.c);
    	for (int m = 0; m< Matriz.r ; m++) {
    				for (int n = 0; n< Matriz.c; n++)
    					Espejo.mat[m][n]=Matriz.mat[m][n];
    				}
    	for (int i = 0; i< Matriz.c ; i++){
    		    		Matriz.mat[r1][i]=Espejo.mat[r2][i];
    		    		Matriz.mat[r2][i]=Espejo.mat[r1][i];
    	}
    }
    
    public static void MultiplicaEscalarMat(MatDouble Matriz, double num){
    	MatDouble Espejo = new MatDouble(Matriz.r, Matriz.c);
    	for (int m = 0; m< Matriz.r; m++) {
    				for (int n = 0; n< Matriz.c; n++)
    					Espejo.mat[m][n]=Matriz.mat[m][n]*num;
    				}
    				
    				for (int m = 0; m< Matriz.r; m++) {
    					for (int n = 0; n< Matriz.c; n++)
    						Matriz.mat[m][n]=Espejo.mat[m][n];
    				}
    }
    
    public static MatDouble FusionMat(MatDouble Matriz1, MatDouble Matriz2){
    	MatDouble Fusion = new MatDouble(Matriz1.r, Matriz1.c+Matriz2.c);
    	for (int m = 0; m< Fusion.r ; m++) {
    				for (int n = 0; n< Matriz1.c; n++)
    					Fusion.mat[m][n]=Matriz1.mat[m][n];
    					for (int o = Matriz1.c ; o< Fusion.c ; o++)
    						Fusion.mat[m][o]=Matriz2.mat[m][o-Matriz1.c];
    				}
    	return Fusion;
    }
    //corregir o revisar
    public static MatDouble ExtraerMat(MatDouble Matriz, int r, int c){
    	MatDouble Corte = new MatDouble(Matriz.r-r,Matriz.c-c);
    	for (int m = 0; m< Corte.r; m++) {
    				for (int n = 0; n< Corte.c; n++)
    					Corte.mat[m][n]=Matriz.mat[m+r][n+c];
    				}
    				return Corte;
    }
    
    public static double SumaArregloTotal(double a[]){
    	double suma=0;
    	for (int i = 0; i<Array.getLength(a); i++)
    		suma+=a[i];
    	return suma;
    }
    
    //Matriz
    double mat[][];
    //Renglon
    int r;
    //Columna
    int c;
    }
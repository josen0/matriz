/*
	MATRIZ v1.0

	Copyright (C) 2009 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

/*
Clase MatInt
Operaciones Con Matrices Enteras

Metodos:
private static void AnadirElemMat(MatInt Matriz, int r, int c,int valor)
public static void RellenaMat(MatInt Matriz)
public static void ImprimeMat(MatInt Matriz)
public static MatInt MultiplicaMat(MatInt Matriz1, MatInt Matriz2)
public static MatInt SumaMat(MatInt Matriz1,MatInt Matriz2)
public static MatInt IdentidadMat(int r, int c)
public static MatInt TransponerMat(MatInt Matriz)
public static MatInt DeterminanteMat()
*/

public class MatInt{

MatInt(int r, int c){
this.mat = new int[r][c];
this.r = r;
this.c = c;
}

private static void AnadirElemMat(MatInt Matriz, int r, int c,int valor){
Matriz.mat[r][c]=valor;
}

public static void RellenaMat(MatInt Matriz){
for(int i=0;i<Matriz.r;i++){
    		for(int j=0;j<Matriz.c;j++){
			AnadirElemMat(Matriz,i,j,FuncIO.LeerTeclaIntCMD("Matriz ["+(i+1)+","+(j+1)+"]"));
    			//Matriz.mat[i][j]=FuncionesIO.LeerTeclaInt("Matriz ["+(i+1)+","+(j+1)+"]");
    		}
    	}
}

public static void ImprimeMat(MatInt Matriz){
System.out.println("Matriz ["+Matriz.r+"]["+Matriz.c+"]");
for(int i=0;i<Matriz.r;i++){
    		for(int j=0;j<Matriz.c;j++){
    			System.out.print("\t"+Matriz.mat[i][j]);
    			
    		}
    		System.out.print("\n");
    	}
}

public static MatInt MultiplicaMat(MatInt Matriz1, MatInt Matriz2){
if(Matriz1.c==Matriz2.r){
MatInt MatrizResult=new MatInt(Matriz1.r,Matriz2.c);
//MatrizResult.mat=new int[MatrizResult.r][MatrizResult.c];
int sum;
    	for(int i=0;i<MatrizResult.r;i++){
    		for(int j=0;j<MatrizResult.c;j++){
    			sum=0;
    			for(int k=0;k<Matriz1.c;k++){
    				sum+=Matriz1.mat[i][k]*Matriz2.mat[k][j];
    			}
			AnadirElemMat(MatrizResult,i,j,sum);
    			//MatrizResult.mat[i][j]=sum;
    		}
    	}
return MatrizResult;
}else{
System.out.print("Matrices no multiplicables, dimensiones erroneas\n");
return null;
}
}

public static MatInt SumaMat(MatInt Matriz1,MatInt Matriz2){
	if(Matriz1.r==Matriz2.r && Matriz2.c==Matriz1.c){
		MatInt Matriz = new MatInt(Matriz1.r,Matriz2.c);
			for(int i=0;i<Matriz.r;i++){
				for(int j=0;j<Matriz.c;j++){
					AnadirElemMat(Matriz,i,j,Matriz1.mat[i][j]+Matriz2.mat[i][j]);
					}
				}
			return Matriz;
	}else{
		System.out.print("Dimensiones Incorrectas");
		return null;
	}
}


public static MatInt IdentidadMat(int r, int c){
	MatInt Matriz = new MatInt(r,c);
	for(int i=0;i<r;i++){
		for(int j=0;j<c;j++){
			if(i==j)
				AnadirElemMat(Matriz,i,j,1);
				else
					AnadirElemMat(Matriz,i,j,0);
		}
	}
	return Matriz;
}

public static MatInt TransponerMat(MatInt Matriz){
	MatInt MatrizT = new MatInt(Matriz.c,Matriz.r);
	for(int i=0;i<MatrizT.r;i++){
		for(int j=0;j<MatrizT.c;j++)
		AnadirElemMat(MatrizT,i,j,Matriz.mat[j][i]);
	}
	return MatrizT;
}

public static int DeterminanteMat(MatInt Matriz){
	int l;
	int det;
	MatInt MatDet = new MatInt(Matriz.r,Matriz.c);
	det=Matriz.mat[0][0];
   for(int k=0; k< Matriz.r; k++){
   	l=k+1;
   	for(int i=l;i< Matriz.r;i++){
   		for(int j=l;j<=Matriz.c;j++){
   			AnadirElemMat(MatDet,i,j,(Matriz.mat[k][k]*Matriz.mat[i][j]-Matriz.mat[k][j]*Matriz.mat[i][k])/Matriz.mat[k][k]);
   			det*=MatDet.mat[k+1][k+1];
   		}
   	}
   }
   return det;
}

/*
//Probando la clase
public static void main(String[] argv){

MatInt matriz1 = new MatInt(2,2);

MatInt matriz2 = new MatInt(2,1);

RellenaMat(matriz1);
RellenaMat(matriz2);

MatInt res = MultiplicaMat(matriz1,matriz2);

ImprimeMat(res);

}
*/
//Matriz
int mat[][];
//Renglones
int r;
//Columnas
int c;
}
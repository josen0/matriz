/*
	MATRIZ v1.0

	Copyright (C) 2009 J. Nieto, all rights reserved.
	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
   	the Free Software Foundation; either version 3, or (at your option)
   	any later version.

   	This program is distributed in the hope that it will be useful,
   	but WITHOUT ANY WARRANTY; without even the implied warranty of
   	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   	GNU General Public License for more details.

 	You should have received a copy of the GNU General Public License
    	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	contact: kkeezff@hotmail.com

*/

/**
CMD
 */
 import java.lang.reflect.Array;
public class CMD {

    public CMD() {
    	
    }
    public static void main (String[] args) {
    	System.out.println("v1.0");
    	FuncIO.LeerTeclaIntCMD("Dame un numero");
    	MatInt x = new MatInt(5,6);
    	int c=Array.getLength(x.mat);
    	System.out.println(c);
    	MatInt d =MatInt.TransponerMat(x);
    	c=Array.getLength(d.mat);
    	System.out.println(c);
    }
    	
}